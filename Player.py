import copy

class Player:
    
    def __init__(self, name):
        self.name = name
        self.hand = []
        
    def addToHand(self, card):
        self.hand.append(card)
    
    def showHand(self):
        cards = ''
        for card in self.hand:
            cards = cards + '| ' + card.__repr__() + ' '
        cards = cards + '|'
        return cards
    
    def checkHand(self):
        return len(self.hand)
        
    def resetHand(self, options):
        if(len(options)):
            for card in options:
                self.addToHand(card)   
                
    def checkOptions(self, faceUpCard, hasPutDown, suitToMatch, matchedSuit):
        options = []
        hand = copy.copy(self.hand)
        if(not matchedSuit):
            if(suitToMatch == ''): 
                for card in hand:
                    if((card.suit.repChar() == faceUpCard.suit.repChar()) or (card.face.repChar() == faceUpCard.face.repChar()) or (card.face.repChar() == '8')):
                        if(not hasPutDown):
                            options.append(card)
                            self.hand.remove(card)
                        else:
                            if(card.face.repChar() == faceUpCard.face.repChar()):
                                options.append(card)   
                                self.hand.remove(card)
            else:
                for card in hand:
                    if(card.suit.repChar() == suitToMatch):
                        options.append(card)
                        self.hand.remove(card)
        return options
    
    def showOptions(self, options, hasDrawn, hasPutDown):
        optionNumber = 0
        print("Options:")
        if(len(options)):
            for card in options:
                optionNumber += 1
                print(str(optionNumber) + ". Put down | " + card.__repr__() + " |")
            if(not hasDrawn):
                if((not hasPutDown) and (not hasDrawn)):
                    print(str(len(options) + 1) + ". Draw a card.")
                else:
                    print(str(len(options) + 1) + ". End turn.")
            else:
                print(str(len(options) + 1) + ". End turn.")
        else:
            print("1. Draw a card.")
            
    
    
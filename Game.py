import sys
import time
import random
import os
import platform
from PlayingCards import PlayingCardSuit, PlayingCard, PlayingCardDeck
from Player import Player

members = ["Kegan Adorna", "Jared Dantis", "Brandon Martin", "Hans Mingala", "Derrick Villamayor"]
deck = PlayingCardDeck()
players = []
turn = 0
pile = []
suits_char = ['S', 'H', 'C', 'D']
suits_word = ['Spades', 'Hearts', 'Clubs', 'Diamonds']
suitToMatch = ''
suitToWord = ''
hasWinner = False
winner = ''

def clearScreen():
	OS = platform.system()
	if OS == 'Windows':
		os.system('cls')
	else:
		os.system('clear')

clearScreen()

print("Crazy Eights!\n")
print("Members:")
for member in members:
    print(member)
input("\nPress ENTER to continue...")

clearScreen()

noOfPlayers = int(input("Number of Players: "))
if((noOfPlayers < 2) or (noOfPlayers > 7)):
    while((noOfPlayers < 2) or (noOfPlayers > 7)):
        clearScreen()
        print("There can only be a min. of 2 and a max. of 7 players.")
        noOfPlayers = int(input("Number of players: "))

clearScreen()
        
for count in range(1, noOfPlayers + 1):
    name = input("Name of Player " + str(count) + ": ")
    players.append(Player(name))
    clearScreen()

print("Game commencing...")
time.sleep(3)

clearScreen()

print("Shuffling deck...")
randomNumber = random.randint(1, 10)
for n in range(randomNumber):
    deck.shuffle()
time.sleep(3)

clearScreen()

print("Dealing cards...")
if(noOfPlayers == 2):
    noOfCards = 7
else:
    noOfCards = 5
for count in range(noOfCards):
    for player in players:
        player.addToHand(deck.deal())
time.sleep(3)

clearScreen()

print("Setup complete.")
time.sleep(3)

clearScreen()

input("Press ENTER to start the game...")

while True:
    clearScreen()
    
    activePlayer = players[turn%noOfPlayers]
    
    input("It's your turn, " + activePlayer.name + ".")
    
    clearScreen()
    
    if(suitToMatch == ''):
        print("Revealed Card:")
        if(turn == 0):
            faceUpCard = deck.deal()
            if(faceUpCard.face.repChar() == '8'):
                while(faceUpCard.face.repChar() == '8'):
                    deck.deck.append(faceUpCard)
                    deck.shuffle()
                    faceUpCard = deck.deal()
            pile.append(faceUpCard)
        else:
            faceUpCard = pile[len(pile) - 1]
        print("| " + faceUpCard.__repr__() + " |")    
    else:
        print("Suit to match: " + suitToMatch)
        
    print('Your hand:')
    print(activePlayer.showHand() + '\n')
    
    hasDrawn = False
    hasPutDown = False
    hasFinished = False
    matchedSuit = False
    
    options = activePlayer.checkOptions(faceUpCard, hasPutDown, suitToMatch, matchedSuit)
    
    while (not hasFinished):  
        activePlayer.showOptions(options, hasDrawn, hasPutDown)
        choice = int(input("\nPick option no.: "))
        
        clearScreen()
        
        if((choice == len(options) + 1) and hasDrawn):
            input("You end your turn.")
            activePlayer.resetHand(options)
            options = []
            hasFinished = True
        elif((choice == len(options) + 1) and hasPutDown):
            input("You end your turn.")
            activePlayer.resetHand(options)
            options = []
            hasFinished = True
        elif((choice == len(options) + 1) and (not len(options))):
            newCard = deck.deal()
            print("You drew a | " + newCard.__repr__() + " |\n")
            activePlayer.resetHand(options)
            activePlayer.addToHand(newCard)
            hasDrawn = True
            if(suitToMatch == ''):
                print("Revealed Card:")
                print("| " + faceUpCard.__repr__() + " |")
            else:
                print("Suit to match: " + suitToMatch)
            print("Your hand:")
            print(activePlayer.showHand() + '\n')
            options = activePlayer.checkOptions(faceUpCard, hasPutDown, suitToMatch, matchedSuit)
            if(not len(options)):
                input("You end your turn.") 
                activePlayer.resetHand(options)
                options = []
                hasFinished = True   
        elif(choice == len(options) + 1):
            newCard = deck.deal()
            print("You drew a | " + newCard.__repr__() + " |\n")
            activePlayer.resetHand(options)
            activePlayer.addToHand(newCard)
            hasDrawn = True
            if(suitToMatch == ''):
                print("Revealed Card:")
                print("| " + faceUpCard.__repr__() + " |")
            else:
                print("Suit to match: " + suitToMatch)
            print("Your hand:")
            print(activePlayer.showHand() + '\n')
            options = activePlayer.checkOptions(faceUpCard, hasPutDown, suitToMatch, matchedSuit)
            if(not len(options)):
                input("You end your turn.") 
                activePlayer.resetHand(options)
                options = []
                hasFinished = True
        else:
            playedCard = options.pop(choice - 1)
            print("You put down a | " + playedCard.__repr__() + " |\n")
            if(playedCard.face.repChar() != '8'):
                pile.append(playedCard)
                hasPutDown = True
                activePlayer.resetHand(options)
                options = []
                if(suitToMatch == ''):
                    print("Revealed Card:")
                    print("| " + playedCard.__repr__() + " |")
                else:
                    print("Suit to match: " + suitToMatch)
                print("Your hand:")
                print(activePlayer.showHand() + '\n')
                if(suitToMatch != ''):
                    matchedSuit = True
                    suitToMatch = ''
                options = activePlayer.checkOptions(playedCard, hasPutDown, suitToMatch, matchedSuit)
                if(not len(options)):
                    input("You end your turn.")
                    activePlayer.resetHand(options)
                    options = []
                    hasFinished = True
            else:
                print("Your hand:")
                print(activePlayer.showHand() + '\n')
                print("Pick a suit:")
                print("1. Spades")
                print("2. Hearts")
                print("3. Clubs")
                print("4. Diamonds")
                choice = int(input("\nPick option no.: "))
                suitToMatch = suits_char[choice - 1]
                suitToWord = suits_word[choice - 1]
                input("\nYou chose " + suitToWord + " as the suit to match.")
                hasFinished = True
    
    for player in players:
        if(not player.checkHand()):
            winner = player.name
            hasWinner = True
    
    if(hasWinner):
        break
    
    if(not len(deck.deck)):
        for cardCount in range(len(pile)):
            if(cardCount != len(pile) - 1):
                deck.deck.append(pile[cardCount])
        deck.shuffle()
    
    turn = turn + 1

input(winner + " is the winner!")

clearScreen()

print("Thanks for playing!")
time.sleep(3)

